package com.sample.database.controller;

import com.sample.database.repository.GreetingRepository;
import com.sample.database.entities.Greeting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

@RestController
public class GreetingController {

    private final GreetingRepository greetingRepository;

    public GreetingController(GreetingRepository greetingRepository) {
        this.greetingRepository = greetingRepository;
    }

    @GetMapping("/greeting")
    public String greeting() {
        return getRandomGreeting();
    }

    private String getRandomGreeting() {
        List<Greeting> greetings = this.greetingRepository.findAll();
        Random random = new Random();
        int randomListIndex = random.nextInt(greetings.size());
        return greetings.get(randomListIndex).getName();
    }

}
