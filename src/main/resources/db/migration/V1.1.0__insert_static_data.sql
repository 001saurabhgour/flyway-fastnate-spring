INSERT INTO greeting (id, name) VALUES (1, 'Hello!');
INSERT INTO greeting (id, name) VALUES (2, 'Good morning.');
INSERT INTO greeting (id, name) VALUES (3, 'Good afternoon.');
INSERT INTO greeting (id, name) VALUES (4, 'Good evening.');
INSERT INTO greeting (id, name) VALUES (5, 'It''s nice to meet you.');
INSERT INTO greeting (id, name) VALUES (6, 'It''s a pleasure to meet you.');